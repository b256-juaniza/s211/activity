console.log("Hello Universe");

// An array in programming is simply a list of data

let studentNumberA = '2023-1923'
let studentNumberB = '2023-1924'
let studentNumberC = '2023-1925'
let studentNumberD = '2023-1926'
let studentNumberE = '2023-1927'

// With an array, we can simply write the code like this:
let studentNumbers = ['2023-1923', '2023-1924'];

/*
    - Arrays are used to store multiple related values in a single variable
    - They are declared using square brackets ([]) also known as "Array Literals"
    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
    - Arrays also provide access to a number of functions/methods that help in achieving this
    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific ob
	- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
    - Arrays are also objects which is another data type
    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
    - Syntax
        let/const arrayName = [elementA, elementB, Elem
- Syntax
        let/const arrayName = [elementA, elementB, ElementC...]
*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['acer', 'Asus', ]

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);
let currentLaker = lakersLegends[2];
console.log(currentLaker);
console.log(lakersLegends);
lakersLegends[2] = 'Pau Gasol';
console.log("Array after reassignment");
console.log(lakersLegends);

let lastIndexElement = lakersLegends.length - 1;

console.log(lakersLegends[lastIndexElement]);