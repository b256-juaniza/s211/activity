console.log("Hello Universe");

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

let currentIndex = users.length - 1;
/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
	function addElement(element){
		users[users.length++];
		users[users.length-1] = element;
	};

	addElement(prompt("Add an Element to the Array: "));
	console.log(users);

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/	
	
	let itemFound;

	function getIndex(num1) {
		if (num1 > currentIndex) {
			while (num1 > currentIndex){

				alert("You can't get an Element that is not in the Arrays!");
				num1 = getIndex(prompt("Input a Number in the Range of the Arrays Index to get the Element. \nThe Current Arrays Index length is: 0 - " + currentIndex));
			}
		}
		return itemFound = num1;
	}

	getIndex(parseInt(prompt("Input a Number in the Range of the Arrays Index to get the Element. \nThe Current Arrays Index length is: 0 - " + currentIndex)));
	console.log("You picked \"" + users[itemFound] + "\"");
/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.
*/
	let backup;
	function removeElement() {
		backup = users[users.length-1];
		users.length--;
		return console.log("The Admin removed \""+ backup + "\"");
	}
	removeElement();
/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/
	let num = parseInt(prompt("Enter between 0 - "+ parseInt((users.length) - 1) + " to choose a Specific Element to change: "));
	let newElement;

	if (num > parseInt((users.length) - 1)) {
		while (num > parseInt((users.length) - 1)) {

		 	alert("You can't change an Element that is not in the Array!");
		 	num = parseInt(prompt("Enter between 0 - "+ parseInt((users.length) - 1) + " to choose a Specific Element to change: "));
			}
		}

	newElement = prompt("Enter the Name you want to change \"" + users[num] + "\" into:");

	function updateArray(index, name) {
		users[index] = name;
	}

	updateArray(num, newElement);
	console.log(users);
/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

	function clearArray(){
		for (let i = users.length; i != 0; i--) {
		users.length--;
		}
	}

	clearArray();
/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
	let isUsersEmpty = check();
	function check(){
		if (users.length > 0) {
			return false;
		}	
		else {
			return true;
		}
	}
	check();
	console.log(users);
	console.log(isUsersEmpty);